# Two Stage Operational Amplifier Design
The project is a design of a two stage operational amplifier that meets specific requirements on gain and bandwidth.

Used LTspice simulation software well as theoretical knowledge of MOSFETS in IC design to Build a two stage operational amplifier using 180 nm CMOS technology, controlling sizes of transistors in use to produce the desired gain and bandwidth.